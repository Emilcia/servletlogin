package com.example.servletjspdemo.filters;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.servletjspdemo.domain.Person;
import com.example.servletjspdemo.domain.User;

public class adminFilter implements Filter {

	FilterConfig filterConfig = null;

	private ServletContext context;

	public void init(FilterConfig config) throws ServletException {
		this.context= config.getServletContext();
		this.filterConfig = config;
	}
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain arg2) throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		PrintWriter out = resp.getWriter();
		
	HashMap<String,Person> osoby = (HashMap<String,Person>) context.getAttribute("osoby");
	
		
		if (osoby.get(req.getSession().getAttribute("username")).getUser()!=User.admin)
		{
			resp.sendRedirect("profil");
		}
		else 
		{
			arg2.doFilter(req, resp);
		}
		
		out.close();
		
	}



}
