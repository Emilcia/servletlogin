package com.example.servletjspdemo.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.servletjspdemo.domain.Person;

@WebServlet(urlPatterns = "/panel")
public class panelServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
		
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		ServletContext app = getServletContext();

		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		out.println("<html><body><h2>Simple form servlet</h2>" +
				"<form action='profile'>" +
				"Login: <br><input type='text' name='firstName' /> <br />" +
				"<select name = 'opcja'> "
				+ "<option value='1'>admin</option>"
				+ "<option value='2'>zwykly</option>"
				+ "<option value='3'>premium</option>"+
				"</select>" +
				"<input type='submit' value=' Ok ' />" +
				"</form>" +
				"</body></html>");
		out.close();
		
		HashMap<String, Person> osoby = (HashMap<String, Person>) app.getAttribute("osoby");

	}

}
