package com.example.servletjspdemo.web;

	import java.io.IOException;
	import java.io.PrintWriter;

	import javax.servlet.ServletException;
	import javax.servlet.annotation.WebServlet;
	import javax.servlet.http.HttpServlet;
	import javax.servlet.http.HttpServletRequest;
	import javax.servlet.http.HttpServletResponse;

	@WebServlet(urlPatterns = "/oops")
	public class errorServlet extends HttpServlet {

		private static final long serialVersionUID = 1L;
		
		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
			
			response.setContentType("text/html");
			
			PrintWriter out = response.getWriter();
			
			out.println("<html><body><h2>Blad</h2>" +
			"Istnieje juz uzytkownik o takich samych danych."+
					"<br><a href='index.jsp'>Return</a>"+
				"</body></html>");
			out.close();
		}

	}

