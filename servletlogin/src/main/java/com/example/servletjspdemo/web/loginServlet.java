package com.example.servletjspdemo.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.servletjspdemo.domain.Person;
import com.example.servletjspdemo.domain.User;

@WebServlet(urlPatterns = "/login")
public class loginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	@Override
	public void init() throws ServletException {
		
		ServletContext app = getServletContext();

		 HashMap<String, Person> osoby = new HashMap<String, Person>();
		 	Person person = new Person();
		 	person.setFirstName("admin");
		 	person.setPassword("qwerty");
		 	person.setUser(User.admin);
		 	osoby.put("admin", person);
		 	
		 	app.setAttribute("osoby", osoby);
	}
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		out.println("<html><body><h2>Simple form servlet</h2>" +
				"<form action='profile'>" +
				"Login: <br><input type='text' name='login' /> <br />" +
				"Haslo: <br><input type='password' name='haslo' /> <br />" +
				"<input type='submit' value=' Zaloguj sie ' />" +
				"<p><a href='form'>Zarejestruj sie!</a></p>" +
				"</form>" +
				"</body></html>");
		out.close();
	}

}
