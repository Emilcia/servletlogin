package com.example.servletjspdemo.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.servletjspdemo.domain.Person;
import com.example.servletjspdemo.domain.User;

@WebServlet(urlPatterns = "/profile")
public class profileServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		ServletContext app = getServletContext();
		response.setContentType("text/html");
		
		HashMap<String, Person> osoby = (HashMap<String, Person>)app.getAttribute("osoby");
		
	//	request.getSession().setAttribute("username", request.getParameter("login").toString());
		
		PrintWriter out = response.getWriter();
		out.println("<html><body><h2>Simple form servlet</h2>" +
				"<p><a href='panel'>Panel administracyjny</a></p>" +
				"<p><a href='login'>Wyloguj sie</a></p>" +
				"</body></html>");
		
			for(Person a : osoby.values())
				{
					out.println(a.getFirstName());
				}
			
		out.close();
	}

}