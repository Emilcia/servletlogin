package com.example.servletjspdemo.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.servletjspdemo.domain.Person;
import com.example.servletjspdemo.domain.User;

@WebServlet(urlPatterns = "/data")
public class DataServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	ServletContext app = getServletContext();

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		request.getSession().setAttribute("name", "");
		
				
		PrintWriter out = response.getWriter();
		
		out.println("<html><body><h2>Your data</h2>" +
				"<p>Login: " + request.getParameter("login") + "<br />" +
				"<p>Haslo: " + request.getParameter("haslo") + "<br />" +
				"<p>Email: " + request.getParameter("email") + "<br />" +
				"<p><a href='login'>Return</a></p>" +
				"</body></html>");
		out.close();
		
		Person osoba = new Person();
		osoba.setFirstName(request.getParameter("login"));
		osoba.setPassword(request.getParameter("haslo"));
		osoba.setUser(User.zwykly);
		
		((HashMap<String, Person>) app.getAttribute("osoba")).put(osoba.getFirstName(), osoba);
		
	}

}
